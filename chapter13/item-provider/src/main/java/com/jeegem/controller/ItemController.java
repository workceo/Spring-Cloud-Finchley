package com.jeegem.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
public class ItemController {
	
	@HystrixCommand(fallbackMethod="itemError")
	@RequestMapping("/item")
	public String item(String name) {
		return "success..."+name;
	}
	
	public String itemError(String name) {
		return "item provider hystrix error....";
	}
	
}
