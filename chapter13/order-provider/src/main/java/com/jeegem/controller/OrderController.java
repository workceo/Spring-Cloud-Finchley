package com.jeegem.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
public class OrderController {
	
	@HystrixCommand(fallbackMethod="itemError")
	@RequestMapping("/order")
	public String item(String name) {
		return "success..order."+name;
	}
	
	public String itemError(String name) {
		return "order provider hystrix error....";
	}
	
}
