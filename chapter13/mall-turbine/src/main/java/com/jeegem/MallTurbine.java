package com.jeegem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

@SpringBootApplication
@EnableTurbine
public class MallTurbine {

	public static void main(String[] args) {
		SpringApplication.run(MallTurbine.class, args);
	}

}
