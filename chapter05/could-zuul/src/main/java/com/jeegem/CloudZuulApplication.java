package com.jeegem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
/*
 * 路由器：
 * 就是转发，@EnableZuulProxy标识为路由器
 */
@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy
public class CloudZuulApplication {
	public static void main(String[] args) {
		SpringApplication.run(CloudZuulApplication.class, args);
	}
}
