package com.jeegem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jeegem.service.HelloService;

@RestController
public class FeignController {
	
	@Autowired
	HelloService helloService;
	
	@RequestMapping("feignClient")
	public String feignClient(String name) {
		return helloService.hello(name);
	}
	
}
