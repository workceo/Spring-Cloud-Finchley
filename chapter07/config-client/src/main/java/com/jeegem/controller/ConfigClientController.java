package com.jeegem.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigClientController {
	
	@Value("${foo}")
	String foo;
	
	@Value("${projectName}")
	String projectName;
	
	@RequestMapping("/getConfig")
	public String getConfig() {
		return foo+" "+projectName;
	}
	
}
