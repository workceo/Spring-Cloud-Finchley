package com.jeegem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ProviderController {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@RequestMapping("/provider")
	public String provider() {
		return "provider-zipkin";
	}
	
	@RequestMapping("/getClient")
	public String getClient() {
		return restTemplate.getForObject("http://localhost:8988/client", String.class);
	}
	
}
