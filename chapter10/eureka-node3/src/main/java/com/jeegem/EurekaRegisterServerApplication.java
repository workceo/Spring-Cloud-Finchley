package com.jeegem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

//@EnableEurekaServer标注为eureka注册中
@SpringBootApplication
@EnableEurekaServer
public class EurekaRegisterServerApplication {
	public static void main(String[] args) {
		SpringApplication.run(EurekaRegisterServerApplication.class, args);
	}
}
